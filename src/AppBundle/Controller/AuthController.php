<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;

use Symfony\Component\HttpFoundation\Session\Session;


class AuthController extends Controller
{
	/**
     * @Route("/usuarios/registro", name="registro")
     */
    public function registerAction(Request $request)
    {
        $sugar = $this->get('app.sugarcrm');

        //Creamos el formulario
        $user = new User();
        $form = $this->createForm(UserType::class, $user);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            

            // Peticion a sugar
            //Comprobar que el usuario no exista en el crm.
            $isInCrm = $sugar->retrieveRecordsByModule('Contacts', [
                'filter' => [
                    [
                        'email1' => $user->getEmail(),
                    ]
                ]
            ]);   
            
            if (empty($isInCrm->records)) {

                //Registrar el usuario en crm y recoger la id de crm

                $userCRM = $sugar->createRecordByModule('Contacts', array('email1' => $user->getEmail()));

                if (isset($userCRM->id)) {
                    // crm_id recibido que meteremos en el objeto user para añadirlo a la bbdd de la web
                    $user->setCrmId($userCRM->id);    
                }
                
                //Registramos al usuario 
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                //Actualizamos en CRM con su ID Web
                $sugar->updateRecordByModule('usr_WebUsers', $user->getCrmId(), [
                    'symfonyid_c' => $user->getId()
                ]);

                return $this->render(
                    'default/web/registro.html.twig',
                    array('form' => $form->createView(), 'msg_type' => 'success', 'msg' => 'Usuario Registrado')
                );
            } else {

                //Devolvemos error indicando que el usuario ya existe
                return $this->render(
                    'default/web/registro.html.twig',
                    array('form' => $form->createView(), 'msg_type' => 'danger', 'msg' => 'El usuario indicado ya existe')
                );
            }
            
        }

        return $this->render(
            'default/web/registro.html.twig',
            array('form' => $form->createView())
        );
    }


    /**
     * @Route("/usuarios/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'default/web/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }
} 