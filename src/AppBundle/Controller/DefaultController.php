<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;


use Symfony\Component\HttpFoundation\Session\Session;


class DefaultController extends Controller
{
    
    private $session;

    public function __construct() 
    {
        $this->session = new Session();
       
    }

    /**
     * @Route("/", name="home")
     */
    public function homeAction(Request $request)
    {
        return $this->render('default/web/home.html.twig');
    }

    /**
     * @Route("/producto/{id}", name="producto")
     */
    public function productoAction(Request $request, $id="")
    {

        $sugar = $this->get('app.sugarcrm');
        $product = $sugar->retrieveRecordByModuleId('daw_Productos', $id);
        if (isset($product->error)) {
            $product = [];
        }

        return $this->render('default/web/producto.html.twig', array('producto' => $product));
    }

    /**
     * @Route("/productos", name="productos")
     */
    public function productosAction(Request $request)
    {
        $sugar = $this->get('app.sugarcrm');


        $productos = $sugar->retrieveRecordsByModule('daw_Productos'); //Prueba
        // replace this example code with whatever you need
        return $this->render('default/web/productList.html.twig', array('productos' => $productos->records));
    }

    /**
     * @Route("/checkout", name="checkout")
     */
    public function checkoutAction(Request $request)
    {   
        $sugar = $this->get('app.sugarcrm');
        if (!$this->getUser()) {
            return $this->redirectToRoute('login');
        }
        $crmID = $this->getUser()->getCrmId();
		$personalData = $sugar->retrieveRecordByModuleId('Contacts', $crmID);
        //Montamos array de datos para pasarlos a la plantilla
		$userData['first_name'] = $personalData->first_name;
		$userData['last_name'] = $personalData->last_name;
		$userData['email'] = $personalData->email1;
		$userData['username'] = $this->getUser()->getUsername();
		$userData['phone_mobile'] = $personalData->phone_mobile;
		$userData['primary_address_street'] = $personalData->primary_address_street;
		$userData['primary_address_city'] = $personalData->primary_address_city;
		$userData['primary_address_state'] = $personalData->primary_address_state;
		$userData['primary_address_postalcode'] = $personalData->primary_address_postalcode;
		$userData['primary_address_country'] = $personalData->primary_address_country;

        $cart = $this->session->get('cart');
        $ids = array_keys($cart);
        
        $productos = [];
        foreach ($cart as $id_cart => $cantidad_cart) {
            $productos[] = $sugar->retrieveRecordByModuleId('daw_Productos', $id_cart);
        }

        return $this->render(
            'default/web/checkout.html.twig', array('productos' => $productos, 'user' => $userData)
        );
    }

    /**
     * @Route("/confirm_order", name="confirm_order")
     */
    public function confirm_orderAction(Request $request)
    {   
        $cart = $this->session->get('cart');
        if ($cart == '') {
            return $this->redirectToRoute('home');
        }
    
        $method = $request->request->get('paymentMethod');

        $sugar = $this->get('app.sugarcrm');
        // Crear pedido
        $pedido = [];
        $linPed = [];
        $mensaje = '';
        $crmID = $this->getUser()->getCrmId();
        $params = array(
            'first_name' => $request->get('firstName'),
            'last_name' => $request->get('lastName'),
            'primary_address_street' => $request->get('address'),
            'primary_address_city' => $request->get('city'),
            'primary_address_country' => $request->get('country'),
            'primary_address_postalcode' => $request->get('zip'),
            'primary_address_state' => $request->get('state')
        );

        //Llamada a sugar para editar la informacion
        $response = $sugar->updateRecordByModule('Contacts', $crmID, $params);
        
        $pedido = $sugar->createRecordByModule('ped_Pedido', array(
            'contacts_ped_pedido_1contacts_ida' => $this->getUser()->getCrmId(),
            'estado_c' => 'pagado',
            'metodo_c' => $method
        ));
        
        if (isset($pedido->error)) {
            $mensaje = 'No se ha podido crear el pedido';
        } else {
            // Creamos lineas pedido            
            foreach ($cart as $id => $talla_array) {
                $producto = $sugar->retrieveRecordByModuleId('daw_Productos', $id);
                foreach ($talla_array as $talla => $color_array) {
                    foreach ($color_array as $color => $qty) {
                        $linPed[] = $sugar->createRecordByModule('ped_LineasPedido', array(
                            'name' => $producto->name,
                            'ped_pedido_ped_lineaspedido_1ped_pedido_ida' => $pedido->id,
                            'daw_productos_id_c' => $producto->id,
                            'cantidad_c' => $qty,
                            'talla_c' => $talla,
                            'color_c' => $color,
                            'subtotal_c' => $producto->price_c * $qty
                        ));
                    }
                }                
            }
            $mensaje = 'El pago se ha realizado correctamente';
            $this->session->set('cart', '');
        }
        
        return $this->render(
            'default/web/confirmed.html.twig', array('pedido' => $pedido, 'lineas' => $linPed, 'mensaje' => $mensaje)
        );
    }

    /**
     * @Route("/cart", name="cart")
     */
    public function cartAction(Request $request)
    {
        // info del form
        $id = $request->request->get('id');
        $cantidad = $request->request->get('cantidad');
        $talla = $request->request->get('talla');
        $color = $request->request->get('color');

        // update variable session Cart
        $cart = $this->session->get('cart');

        if ($cart == NULL) {
            $cart = [];
        }
        if ($id != NULL && $cantidad != NULL && $talla != NULL && $color != NULL) {
            if (isset($cart[$id][$talla][$color])) {
                $cart[$id][$talla][$color] = $cart[$id][$talla][$color] + $cantidad;
            } else {
                $cart[$id][$talla][$color] = $cantidad;
            }
            $this->session->set('cart', $cart);
        }

        // recuperamos info productos
        $productos = [];
        $sugar = $this->get('app.sugarcrm');
        foreach ($cart as $id_cart => $cantidad_cart) {
            $productos[] = $sugar->retrieveRecordByModuleId('daw_Productos', $id_cart);
        }

        return $this->render(
            'default/web/cart.html.twig', array('productos' => $productos)
        );
    }

    /**
     * @Route("/cart_update", name="cart_update")
     */
    public function cart_updateAction(Request $request)
    {
        // info del form
        
        $id = $request->request->get('id');
        $qty = $request->request->get('qty');
        $talla = $request->request->get('talla');
        $color = $request->request->get('color');
        $submit = $request->request->get('submit');
        $cart = $this->session->get('cart');


        if ($submit == "delete") {
            unset($cart[$id][$talla][$color]);
            if (sizeof($cart[$id][$talla]) == 0) {
                unset($cart[$id][$talla]);
                if (sizeof($cart[$id]) == 0) {
                    unset($cart[$id]);
                }
            }
        } else if ($submit == "update") {
            $cart[$id][$talla][$color] = $qty;
        }

        $this->session->set('cart', $cart);

        return $this->redirectToRoute('cart');
    }

}