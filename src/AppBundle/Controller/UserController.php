<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;

use Symfony\Component\HttpFoundation\Session\Session;


class UserController extends Controller
{

	/**
     * @Route("/usuarios/profile", name="profile")
     */
    public function profileAction(Request $request)
    {
    	$sugar = $this->get('app.sugarcrm');
    	$userData = array();

    	//Recupera los datos personales del usuario 
        $user = $this->getUser();
        if ($user == '') {
            return $this->redirectToRoute('login');
        } 

		$crmID = $this->getUser()->getCrmId();
        if(empty($crmID)) {
            return $this->redirectToRoute('login');
        }

		$personalData = $sugar->retrieveRecordByModuleId('Contacts', $crmID);
		
		//Montamos array de datos para pasarlos a la plantilla
		$userData['first_name'] = $personalData->first_name;
		$userData['last_name'] = $personalData->last_name;
		$userData['email'] = $personalData->email1;
		$userData['username'] = $this->getUser()->getUsername();
		$userData['phone_mobile'] = $personalData->phone_mobile;
		$userData['primary_address_street'] = $personalData->primary_address_street;
		$userData['primary_address_city'] = $personalData->primary_address_city;
		$userData['primary_address_state'] = $personalData->primary_address_state;
		$userData['primary_address_postalcode'] = $personalData->primary_address_postalcode;
		$userData['primary_address_country'] = $personalData->primary_address_country;

    	//Recupera 
    	return $this->render('default/user/profile.html.twig', array('userdata' => $userData));
  
    }

    /**
     * @Route("/usuarios/profile/edit", name="profile_edit")
     */
    public function updateprofileAction (Request $request)
    {
    	$sugar = $this->get('app.sugarcrm');
        
        $user = $this->getUser();
        if ($user == '') {
            return $this->redirectToRoute('login');
        } 

    	$crmID = $this->getUser()->getCrmId();	
	    $userData = array();

        if(empty($crmID)) {
            return $this->redirectToRoute('login');
        }
    	
    	if(isset($_POST['save'])) {
    		//Añadimos los campos a los parametros y actualizamos el registro en crm
    		$params = array(
    			'first_name' => $request->get('first_name'),
    			'last_name' => $request->get('last_name'),
    			'phone_mobile' => $request->get('phone_mobile'),
    			'primary_address_street' => $request->get('primary_address_street'),
    			'primary_address_city' => $request->get('primary_address_city'),
    			'primary_address_country' => $request->get('primary_address_country'),
    			'primary_address_postalcode' => $request->get('primary_address_postalcode'),
    			'primary_address_state' => $request->get('primary_address_state')
    		);

    		//Llamada a sugar para editar la informacion
    		$response = $sugar->updateRecordByModule('Contacts', $crmID, $params);
    		
    		//Redireccionamos al perfil de usuario
    		return $this->redirectToRoute('profile');
    	} else if (isset($_POST['cancel'])) {
    		//Redireccionamos al perfil de usuario
    		return $this->redirectToRoute('profile');
    	} else {

	    	//Recupera los datos personales del usuario    	
			$personalData = $sugar->retrieveRecordByModuleId('Contacts', $crmID);
			
			//Montamos array de datos para pasarlos a la plantilla
			$userData['first_name'] = $personalData->first_name;
			$userData['last_name'] = $personalData->last_name;
			$userData['email'] = $personalData->email1;
			$userData['username'] = $this->getUser()->getUsername();
			$userData['phone_mobile'] = $personalData->phone_mobile;
			$userData['primary_address_street'] = $personalData->primary_address_street;
			$userData['primary_address_city'] = $personalData->primary_address_city;
			$userData['primary_address_state'] = $personalData->primary_address_state;
			$userData['primary_address_postalcode'] = $personalData->primary_address_postalcode;
			$userData['primary_address_country'] = $personalData->primary_address_country;
			return $this->render('default/user/editprofile.html.twig', array('userdata' => $userData));
    	}
    	
    	return $this->redirectToRoute('profile');
    	
    }


    /**
     * @Route("/usuarios/orders", name="orders")
     */
    public function ordersAction (Request $request)
    {
    	$sugar = $this->get('app.sugarcrm');

        $user = $this->getUser();
        if ($user == '') {
            return $this->redirectToRoute('login');
        } 

    	$crmID = $this->getUser()->getCrmId();
        
        if(empty($crmID)) {
           return $this->redirectToRoute('login');
        }

    	//Recupera todos los pedidos que haya hecho el usuario
        $pedidos = $sugar->getRelatedRecordsFromModule('Contacts', $crmID, 'contacts_ped_pedido_1');
        return $this->render('default/user/orders.html.twig', array('pedidos' => $pedidos));
    }


    /**
     * @Route("/usuarios/orderdetail", name="order_detail")
     */
    public function orderdetailAction (Request $request)
    {
    	$sugar = $this->get('app.sugarcrm');

        $user = $this->getUser();
        if ($user == '') {
            return $this->redirectToRoute('login');
        } 

    	$crmID = $this->getUser()->getCrmId();

        if(empty($crmID)) {
           return $this->redirectToRoute('login');
        }

        //Recupera todos las lineas de pedido del pedido actual
        $pedido = $sugar->getRelatedRecordsFromModule('ped_Pedido', $request->get('id'), 'ped_pedido_ped_lineaspedido_1');

    	
        return $this->render('default/user/orderdetail.html.twig', array('pedido' => $pedido));
    }
}