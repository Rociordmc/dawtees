<?php
namespace AppBundle\Utils;

//https://symfony.com/doc/current/session.html#basic-usage
/*https://symfony.com/doc/3.4/controller.html#managing-the-session*/
/*Al no poder acceder a la sesion desde la clase, implementamos la clase SessionInterface, ya que la clase Session tambien la implementa.*/
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class SugarCRM
{

	protected $sugar_baseurl; //URL del CRM hacia donde apuntará la API
	protected $sugar_username; //usuario de sugar
	protected $sugar_password; //contraseña del usuario de sugar

	private $_client_id = 'sugar'; 	//Clave ID de OAUth para la autenticación
	private $_client_secret = ''; 	//Clave Secret de OAuth para la autenticación
	private $_platform = 'api';		//plataforma interna de SugarCRM que efectúa la conexión. En nuestro caso la hemos llamado api
	private $session;

	public function __construct($sugar_baseurl, $sugar_username, $sugar_password, SessionInterface $session)
	{

		$this->sugar_baseurl = $sugar_baseurl . '/rest/v10/';
		$this->sugar_username = $sugar_username;
		$this->sugar_password = $sugar_password;

		$this->session = $session;
	}


	
	//Llamamos al endpoint /oauth2/token definido en http://localhost/sugarcrm/rest/v10/help
	/*
	EJEMPLO: 
	Request for Password Grant Types

	{
	   "grant_type":"password",
	   "client_id":"sugar",
	   "client_secret":"",
	   "username":"admin",
	   "password":"password",
	   "platform":"base"
	}
	*/
	private function _login()
	{

		

		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_URL => $this->sugar_baseurl . '/oauth2/token',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => [
				'Content-type: application/json'
			]
		]);

		//parametros que necesitamos para obtener el token
		$params = [
			'grant_type' 	=> 'password',
			'client_id' 	=> $this->_client_id,
			'client_secret' => $this->_client_secret,
			'username' 		=> $this->sugar_username,
			'password' 		=> $this->sugar_password,
			'platform' 		=> $this->_platform,
			
		];

		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		curl_close($curl);

		$response = json_decode($response);

		if(isset($response->access_token)){
			$this->session->set('sugar_access_token', $response->access_token);
			$this->session->set('sugar_refresh_token', $response->refresh_token);
		}else{
			throw new \Exception('ERROR: No se ha podido conectar con SugarCRM.');
		}
	}


	//Cada cierto tiempo tenemos que refrescar el token, por lo que se llama a la misma funcion que cuando se hace login, pero cambiando los parametros.

	/*
	Request for Refresh Token Grant Types

	{
	    "grant_type":"refresh_token",
	    "refresh_token":"c1be5132-655b-1ca3-fb44-512e36709871",
	    "client_id":"sugar",
	    "client_secret":"",
	}
	*/
	private function _refreshToken()
	{
		
		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_URL => $this->sugar_baseurl . '/oauth2/token',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => [
				'Content-type: application/json'
			]
		]);

		$params = [
			'client_id' 	=> $this->_client_id,
			'client_secret' => $this->_client_secret,
			'platform' 		=> $this->_platform,
			'grant_type' 	=> 'refresh_token',
			'refresh_token' => $this->session->get('sugar_refresh_token'),
		];

		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		curl_close($curl);

		$response = json_decode($response);

		if(isset($response->access_token)){
			$this->session->set('sugar_access_token', $response->access_token);
			$this->session->set('sugar_refresh_token', $response->refresh_token);
		}else{
			//Si no podemos refrescar el token, forzamos un nuevo login
			$this->_login();

		}
	}

	/*
	Se usa para recuperar los registros del CRM según el módulo indicado y los filtros enviados.
	Por ejemplo: Recuperar todas los Productos cuyo colo sea Rojo y sean camisetas	

	Los filtros se pasan de esta manera (Ejemplo):
		{
		   "filter":[
		      {
		         "color":"red",
		         "type": "t-shirt"
		      }
		   ]
		}
	*/
	public function retrieveRecordsByModule($module = '', $filter = [], $limit = -1)
	{
		$endpoint = '/' . $module;
		if(!empty($filter))
			$endpoint.= '?' . http_build_query($filter);

		$response = $this->_handleCall($endpoint, 'GET', []); //Al ser metodo GET, no hay params
		return $response;
	}



	/*
	Recupera un registro del CRM, teniendo en cuenta el id que se le pasa.
	*/
	public function retrieveRecordByModuleId($module = '', $id = '')
	{
		$endpoint = '/' . $module . '/' . $id;
		$response = $this->_handleCall($endpoint, 'GET', []);
		return $response;
	}

	/*
	Recupera los registros relacionados, como por ejemplo los pedidos de un contacto
	*/
	public function getRelatedRecordsFromModule($module = '', $id = '', $relationship = '')
	{

		$endpoint = '/' . $module . '/' . $id . '/link/' . $relationship;
		$response = $this->_handleCall($endpoint, 'GET', []);

		return $response;
	}


	/*
	Genera un registro en el modulo informado, con los parametros informados
	*/
	public function createRecordByModule($module = '', $params = [])
	{
		$endpoint = '/' . $module;
		$response = $this->_handleCall($endpoint, 'POST', $params); 
		return $response;
	}

	/*
	Actualiza los campos informados (paramas) del registro indicado (id)
	*/

	public function updateRecordByModule($module = '', $id = '', $params = [])
	{
		$endpoint = '/' . $module . '/' . $id;
		$response = $this->_handleCall($endpoint, 'PUT', $params);
		return $response;
	}



	private function _handleCall($endpoint = '', $method = 'GET', $params = [])
	{
		try{
			//Autenticamos
			$this->_handleAuthentication();

			$curl = curl_init();
			curl_setopt_array($curl, [
				CURLOPT_URL => $this->sugar_baseurl . $endpoint,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTPHEADER => [
					'OAuth-Token: ' . $this->session->get('sugar_access_token'),
					'Content-type: application/json'
				]
			]);

			switch($method){
				case 'POST':
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
					break;
				case 'GET':
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); //No reconoce bien el metodo de get, por lo que se usa el custom request
					curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
					break;
				case 'PUT':
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					break;
				default:
					break;
			}

			$response = curl_exec($curl);
			$info = curl_getinfo($curl);
			curl_close($curl);

			return json_decode($response);

		} catch(Exception $e){

		}
	}


	private function _handleAuthentication()
	{
		//Si el Access Token o el Refresh Token de sugar estan vacios, llamamos a login y guardamos los dos tokens en sesion
		if(empty($this->session->get('sugar_access_token')) || empty($this->session->get('sugar_refresh_token'))){
			$this->_login();
		}else{
			//Si existen los dos, llamamos a refresh token, para que no se nos pierda la sesion con sugar. 
			$this->_refreshToken();
		}
	}

}